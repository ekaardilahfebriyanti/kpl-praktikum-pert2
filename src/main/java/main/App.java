package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;

import static spark.Spark.get;

public class App {
    public static void main(String[] args){
//        get("/hello", (req, res)->"Hello World");
//        get("/hello/:name", (req, res)->"Hello, "+req.params("name"));
//        get("/hello/:name/:umur/:alamat", (req, res)->{
//            String name = req.params("name");
//            String umur = req.params("umur");
//            String alamat = req.params(":alamat");
//            int umr = Integer.parseInt(umur);
//            return "Hello, "+name+" <br> Umur saya "+umr+" tahun <br>Saya berasal dari : <strong style='color: blue;'>"+alamat+"</strong>";
//        });
//        get("/hello/:a/:operator/:b", (req, res)->{
//            String a = req.params("a");
//            String b = req.params("b");
//            String operator = req.params("operator");
//            int par1 = Integer.parseInt(a);
//            int par2 = Integer.parseInt(b);
//            int hasil;
//            if(operator.equalsIgnoreCase("kali")){
//                hasil = par1*par2;
//                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
//            }else if(operator.equalsIgnoreCase("bagi")){
//                hasil = par1/par2;
//                return hasil;
//            }else if(operator.equalsIgnoreCase("kurang")){
//                hasil = par1-par2;
//                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
//            }else if(operator.equalsIgnoreCase("tambah")){
//                hasil = par1+par2;
//                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
//            }else{
//                return "Maaf masukan operator berupa kata, bukan simbol";
//            }
//        });
//        get("/random", (req, res)->"Masukan pilihan anda pada parameter diURL");
//        get("/random/:a", (req, res)->{
//            String[] arr={"Pulang", "Rapat", "Makan", "Tidur", "Main"};
//            Random r=new Random();
//            int randomNumber=r.nextInt(arr.length);
//           return "Aku suka "+arr[randomNumber];
//        });
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new VelocityTemplateEngine(configuredEngine);
//        get("/hello", (req, res)->{
//            HashMap<String, Object> model = new HashMap<>();
//            String templatePath = "/views/index.vm";
//            return  velocityTemplateEngine.render(new ModelAndView(model, templatePath));
//        });

        //latihan 1
        get("/hello", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            model.put("title", "Keamanan Perangkat Lunak");
            model.put("heading", "Praktikum");
            model.put("paragraph", "Lorem ipsum...");
            String templatePath = "/views/index.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 2
        get("/overflow", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String someInt = req.queryParamOrDefault("someInt", "0");
            int i = Integer.parseInt(someInt);
            byte b = (byte) i;
            model.put("someInt", i);
            model.put("someByte", b);
            String templatePath = "/views/overflow.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });
        //latihan 3
        get("/overflow2", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String someInt = req.queryParamOrDefault("someInt", "0");

            int i = Integer.parseInt(someInt);

            if (i >= Byte.MIN_VALUE && i<= Byte.MAX_VALUE){
                byte b = (byte) i;
                model.put("someInt", i);
                model.put("someByte", b);
            }
            else {
                model.put("someInt", i);
                model.put("someByte", "maaf tidak bisa dikoversi");
            }

            String templatePath = "/views/overflow.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 4
        get("/devision", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String aString = req.queryParamOrDefault("a", "1");
            String bString = req.queryParamOrDefault("b", "1");
            int a = Integer.parseInt(aString);
            int b = Integer.parseInt(bString);
            int c = a/b;
            model.put("a", a);
            model.put("b", b);
            model.put("c", c);
            String templatePath = "/views/devision.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

//        latihan 5
        get("/devision2", (req, res)->{
            HashMap<String, Object> model = new HashMap<>();
            String aString = req.queryParamOrDefault("a", "1");
            String bString = req.queryParamOrDefault("b", "1");
            int a,b,c;
            if(aString.equals("[0-9]*") && bString.equals("[0-9]*")){
                a = Integer.parseInt(aString);
                b = Integer.parseInt(bString);
                model.put("a", a);
                model.put("b", b);
                if(b == 0){

                    model.put("c", "Unlimited!");
                }else {

                    c = a/b;
                    model.put("c", c);
                }

            }else {
                a = 1;
                b=1;
                c = a/b;
                model.put("a", a);
                model.put("b", b);
                model.put("c", c);
            }

            String templatePath = "/views/devision.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 6
        get("float", (req, res)->{
           HashMap<String, Object> model = new HashMap<>();
           String cmString = req.queryParamOrDefault("cm","0");
           double m = 1.0;
           double cm = 0.01;
           int cmInput = Integer.parseInt(cmString);
           double result = (m- cm * cmInput);
           model.put("m", m);
           model.put("cm", cm);
           model.put("cmInput", cmInput);
           model.put("result", result);
           String templatePath = "/views/float.vm";
           return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

    }
}
